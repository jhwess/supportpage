var http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs"),
    port = 8080,
    nodemailer = require("nodemailer");

http.createServer(function(request,response) {
    var uri = url.parse(request.url).pathname,
         filename = path.join(process.cwd(), uri);

    var contentTypesByExtension = {
        '.html': "text/html",
        '.css':  "text/css",
        '.js':   "text/javascript"
    };

    var smtpConfig = {
        host: 'apollo.mingledorffs.com',
        port: 25,
        secure: false
    };
    var transporter = nodemailer.createTransport(smtpConfig);

    transporter.verify(function (error, success) {
        if (error) {
            console.log(error);
        }
        else {
            console.log('Server is ready to take our messages');
        }
    });

    function createAndSendMail(id, add, subj, body) {
        var data = {
            from: add,
            to: "servicedesk@mingledorffs.com",
            subject:subj,
            text: "User affected: " + id + "\n" + body
        };
        transporter.sendMail(data, function (error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log("Message sent.");
            }
        });
    }

    fs.exists(filename, function(exists) {
        if(!exists) {
            response.writeHead(404, {"Content-Type": "text/plain"});
            response.write("404 Not found\n");
            response.end();
            return;
        }

        if(fs.statSync(filename).isDirectory()) filename += '/index.html';

        fs.readFile(filename, "binary", function(err, file) {
            if(err) {
                response.writeHead(500, {"Content-Type": "text/plain"});
                response.write(err + "\n");
                response.end();
                return;
            }

            var headers = {};
            var contentType = contentTypesByExtension[path.extname(filename)];
            if (contentType) headers["Content-Type"] = contentType;
            var body = "";
            request.on('data', function (chunk) {
                body += chunk;
            });
            request.on('end', function () {

                if(body.length > 0) {

                    console.log('POSTed: ' + body);
                    var res = body.split("&");
                    var name = res[0].slice(res[0].search("=") + 1, res[0].length);
                    name = name.replace("+", " ");

                    var address = res[1].slice(res[1].search("=") + 1, res[1].length);
                    address = address.replace("%40", "@");

                    var subject = res[2].slice(res[2].search("=") + 1, res[2].length);
                    subject = subject.replace(/\+/g, " ");

                    var bod = res[3].slice(res[3].search("=") + 1, res[3].length);
                    bod = bod.replace(/\+/g, " ");
                    bod = bod.replace(/%2C/g, ",");
                    bod = bod.replace(/%0D%0A/g, "\n");
                    createAndSendMail(name, address, subject, bod);
                }
                response.writeHead(200);
                response.end();
            });
            response.writeHead(200);
            response.write(file, "binary");
            response.end();
        });
    });

}).listen(parseInt(port, 10));

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
